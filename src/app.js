import React, { Component } from 'react';
import './app.css';

import {Listing} from './listing'
import {Show} from './show'
import {Edit} from './edit'
import {Login} from './login'

class App extends Component {
  render() {
    return (
      <div className="container">
        <Listing />
        <Show />
        <Edit />
        <Login />
      </div>
    );
  }
}

export default App;
