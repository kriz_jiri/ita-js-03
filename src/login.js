import React from 'react'

export class Login extends React.Component {
    render() {
        return (
            <div>
                <form action="">
                    <div className="col-md-4">
                        <h2>Login</h2>
                        <div className="form-group">
                            <label htmlFor="">Username</label>
                            <input type="text" className="form-control"/>
                        </div>
                        <div className="htmlForm-group">
                            <label htmlFor="">Password</label>
                            <input type="password" className="form-control"/>
                        </div>
                        <a href="" type="submit" className="btn btn-default">Login</a>
                    </div>
                </form>
            </div>
        )
    }
}