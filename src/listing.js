import React from 'react'

export class Listing extends React.Component {
    render() {
        return (
            <div>
                <h2>Contacts</h2>

                <div id="first-row" className="row">
                    <div className="col-md-4">
                        <button type="button" className="btn btn-default">New Contact</button>
                    </div>
                    <div className="col-md-4"/>
                    <div className="col-md-4">
                        <input type="text" name="search" className="form-control" placeholder="Search.."/>
                    </div>
                </div>

                <table id="contact-table" className="table table-striped">
                    <tbody>
                        <tr>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Adress</th>
                            <th>Note</th>
                        </tr>
                        <tr>
                            <td><a href="">John Doe</a></td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                        </tr>
                        <tr>
                            <td><a href="">Joe Bloggs</a></td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                        </tr>
                        <tr>
                            <td><a href="">...</a></td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                        </tr>
                        <tr>
                            <td><a href="">...</a></td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}
