import React from 'react'

export class Edit extends React.Component {
    render() {
        return (
            <div>
                <h2>New Contact</h2>
                <form>
                    <div className="col-md-6">
                        <div className="form-group"><label>Name</label>
                            <input type="email" className="form-control" placeholder="Email"/></div>
                        <div className="form-group"><label>Phone</label>
                            <input type="tel" className="form-control" placeholder="Phone"/></div>
                        <div className="form-group"><label>Address</label>
                            <input type="text" className="form-control" placeholder="Address"/></div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group"><label>Note</label>
                            <textarea className="form-control" cols="30" rows="10"/></div>
                    </div>
                </form>
                <a href="" type="submit" className="btn btn-default">Submit</a>
            </div>
        )
    }
}